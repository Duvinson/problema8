/* Requerimientos: Se requiere que el usuarios ingrese una cadena con cualquier caracter.

  Garantiza: Que reciba una cadena de caracteres y separe los números del resto de caracteres,
  generando una cadena que no tiene números y otra con los números que había en la cadena original.*/

#include <iostream>
#include <string.h>
#include <stdlib.h>

using namespace std;

int main()
{
    char cadena[1000]={};//declaracion de la cadena original con un tamaño por defecto
    char cadena_numeros[1000]={};//cadena auxiliar que tendra solo numeros
    char cadena_texto[1000]={};//cadena auxiliar que tendra solo texto

    char *p=cadena, *p_numeros=cadena_numeros, *p_texto=cadena_texto;//declaracion de punteros que me facilitaran moverme por las posiciones de las cadenas

    cout << "Ingrese una cadena" << endl;
    cin >> cadena;
    int l1=strlen(cadena);//intruccion que da la longitud de la cadena original

    for (int i=0; i<l1; i++){//ciclo que me recorre las posiciones de la cadena original
        if(*(p+i)>=48 && *(p+i)<=57){// condicional que verifica si algun caracter es un numero
            //cout << *(p+i)-32 << endl;
            *(p_numeros+i) = *(p+i);//si se cumple la instruccion de arriba a la cadena auxiliar de numeros se le asignara el numero encontrado

        }
        else if (*(p+i)>=65 && *(p+i)<=91 || *(p+i)>=97 && *(p+i)<=122 ){ // condicional que verifica si hay letras en la cadena original
                //cout << *(p+i)-32 << endl;
                *(p_texto+i) = *(p+i);//si se cumple el condicional anterior a la cadena auxiliar de texto se le asiganara el texto encontrado

        }


    }
    cout << "Original = ";
    for(int i=0; i<l1; i++)//ciclo para imprimir la cadena original
        {

            cout<<*(p+i);
        }
    cout << "\n Texto = ";
    for(int i=0; i<l1; i++)//ciclo para imprimira la cadena de solo texto
        {

            cout<<*(p_texto+i);
        }
    cout << ", Numeros = ";
    for(int i=0; i<l1; i++)//ciclo para imprimr la cadena de solo numeros
        {

            cout<<*(p_numeros+i);
        }
         cout<<endl ;
    return 0;
}
